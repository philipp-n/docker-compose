# ◦ Requirements

- Linux (tested on Ubuntu 19)
- docker-compose [Install](https://docs.docker.com/compose/install/)

# ◦ Install

### ▸ `composer require philippn/docker-compose --dev`

### ▸ `vendor/bin/docker`

`NB!!!` If your project's path include any space, execution will fail (`composer BUG`) 

```
vendor/bin/docker.sh: 20: export: <...>: bad variable name
```

## ▸ GIT commit created files


# ◦ USE

### - Docker

`docker/run`

`docker/stop` - current container

`docker/stop-all` - Stop all docker containers running in the background (current machine)


### - Gitlab CI

Update `- exec "echo 'Hello world'"`  in `/.gitlab-ci.yml`

Insert any required command(s) instead of `echo 'Hello world'`, testing purposes

__NB!!!__ Remove `/.gitlab-ci.yml` fil if you do not need `Gitlab CI`
